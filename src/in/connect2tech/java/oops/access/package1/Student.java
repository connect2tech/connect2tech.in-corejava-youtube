package in.connect2tech.java.oops.access.package1;

public class Student {
	
	private int marks = 80;
	
	private void displayMarks() {
		System.out.println(marks);
	}
	
	public String name = "Java";

	public void study() {
		System.out.println(name);
		System.out.println(marks);
	}
	
	public void doAssignment() {
		System.out.println("I do assignment");
	}
	
	public void hodMethod() {
		System.out.println(marks);
	}
	
}
