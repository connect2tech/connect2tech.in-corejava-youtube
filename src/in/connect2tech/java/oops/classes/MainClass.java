package in.connect2tech.java.oops.classes;

public class MainClass {

	public static void main(String[] args) {
		
		
		Person person = new Person();
		person.age = 20;
		person.name = "Java";
		System.out.println(person.age);//20
		person.eat();
		
		/*Person p2 = person;
		p2.age = 25;
		
		System.out.println(p2.age);//25
		System.out.println(person.age);//25
*/		
		
		Person person2 = new Person();
		person2.age = 30;
		person2.name = "Python";
		person2.eat();

	}

}
