package in.connect2tech.java.oops.classes;



public class MyClass {
	
	public static void main(String str[]) {
		MyHouse house = new MyHouse();
		house.area = 100;
		house.color = "blue";
		house.cleanHouse();
		
		MyHouse house2 = new MyHouse();
		house2.area = 150;
		house2.color = "white";
	}

}
