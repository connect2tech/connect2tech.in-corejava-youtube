package in.connect2tech.java.oops.classes;

public class Person {
	int age;
	String name;
	
	void eat() {
		System.out.println("I can eat");
	}
	
	void walk() {
		System.out.println("I can walk");
	}
}
