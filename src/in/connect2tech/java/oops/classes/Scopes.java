package in.connect2tech.java.oops.classes;

class House{
	int A=10;
	
	void method1() {
		int B=20;
		int A = 100;
		
		System.out.println(A);
		System.out.println(B);
		
		/*for(int i=0;i<5;i++) {
			System.out.println(i);
		}*/
		
	}
	
	void method2() {
		int C=30;
		
		//System.out.println(A);
		//System.out.println(C);
		//System.out.println(B);
		
		
		{
			int D=40;
			
			System.out.println(A);
			System.out.println(C);
			System.out.println(D);
		}
		
		//System.out.println(D);
	}
	
}


public class Scopes {
	
	public static void main(String str[]) {
		House house = new House();
		house.method1();
		//house.method2();
	}

}
