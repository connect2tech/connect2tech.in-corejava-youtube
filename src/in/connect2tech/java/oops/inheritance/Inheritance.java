package in.connect2tech.java.oops.inheritance;

class Car{
	String color = "red";
	public void startEngine() {
		System.out.println("startEngine");
	}
}

class Maruti extends Car{
	
	public void displayColor() {
		System.out.println(color);
	}
}

class BMW extends Car{
	
	public void startEngine() {
		System.out.println("startEngine with more power");
	}
}

//overriding in java and how it is implemented.


public class Inheritance {

	public static void main(String[] args) {

		int a = 10;
		Car c;
		
		if(a>0) {
			c = new BMW();
		}else {
			c = new Maruti();
		}
		
		c.startEngine();
		
		
		
	}

}
