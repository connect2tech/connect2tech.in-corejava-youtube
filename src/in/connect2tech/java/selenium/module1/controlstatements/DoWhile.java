package in.connect2tech.java.selenium.module1.controlstatements;

public class DoWhile {

	public static void main(String[] args) {
		
		int i = 0;
		
		do {
			System.out.println(i);
			--i;// i = i -1
		}while(i>0);
		
		System.out.println("End of do while loop");

	}

}
