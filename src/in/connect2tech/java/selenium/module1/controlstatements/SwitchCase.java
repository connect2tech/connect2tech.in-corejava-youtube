package in.connect2tech.java.selenium.module1.controlstatements;

public class SwitchCase {

	public static void main(String[] args) {

		int num = 20;

		switch (num) {

		case 10:
			System.out.println(num);
			break;

		case 20:
			System.out.println(num);
			break;

		case 30:
			System.out.println(num);
			break;

		default:
			System.out.println(num);

		}

	}

}
