package in.connect2tech.java.selenium.module1.controlstatements;

public class WhileLoop {

	public static void main(String[] args) {

		int i = 0;
		
		while(i>0) {
			System.out.println(i);
			--i;//i = i - 1;
		}
		
		System.out.println("End of while loop");
	}

}
