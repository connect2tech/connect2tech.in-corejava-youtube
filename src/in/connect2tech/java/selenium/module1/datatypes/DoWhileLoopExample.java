package in.connect2tech.java.selenium.module1.datatypes;

public class DoWhileLoopExample {

	public static void main(String[] args) {
		//Java Program to demonstrate the example of do-while loop  
		int i=1;
		do
		{
			System.out.println(i);
			i++;
		}
		while(i<5);
		
	}

}
