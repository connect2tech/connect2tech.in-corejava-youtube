package in.connect2tech.java.selenium.module1.datatypes;

public class SwitchStatement {

	public static void main(String[] args) {
		//Java Program to demonstrate the example of switch statements
		int num = 20;
		//Switch Expression
		switch(num)
		{
		//Case statements
		case 10: System.out.println("10");
		break;
		case 20: System.out.println("20");
		break;
		case 30: System.out.println("30");
		break;
		default: System.out.println("Not in 10, 20 or 30");    //Default statement
		}

	}

}
