package in.connect2tech.java.selenium.module1.operators;

public class ArithmeticOperators {

	public static void main(String[] args) {
		
		int a = 10;
		int b = 5;
		
		System.out.println(a+b);
		System.out.println(a-b);
		System.out.println(a*b);
		System.out.println(a/b);
		System.out.println(a%b);

	}

}
