package in.connect2tech.java.selenium.module1.operators;

public class BooleanOperators {

	public static void main(String[] args) {
		 int a = 10;
		 int b = 5;
		 int c = 20;
		 
		 boolean bool1 = a>b;
		 boolean bool2 = a>c;
		 
		 /*System.out.println(bool1);
		 System.out.println(bool2);*/
		 
		 System.out.println(bool1 && bool2);
		 System.out.println(bool1 || bool2);
		 System.out.println(!bool1);
		
	}

}
