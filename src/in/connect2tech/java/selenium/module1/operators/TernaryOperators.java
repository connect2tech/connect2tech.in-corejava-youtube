package in.connect2tech.java.selenium.module1.operators;

public class TernaryOperators {

	public static void main(String[] args) {
		
		int a = 2;
		int b = 1;
		
		int min = (a<b)?a:b;
		System.out.println(min);

	}

}
