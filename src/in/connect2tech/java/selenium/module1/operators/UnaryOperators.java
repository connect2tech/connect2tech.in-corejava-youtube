package in.connect2tech.java.selenium.module1.operators;

public class UnaryOperators {

	public static void main(String[] args) {
		int x = 10;
		int xx = ++x;//Pre Increment
		
		int y = 20;
		int yy = y++;//Post increment
		
		System.out.println(x);
		System.out.println(xx);

		System.out.println(y);
		System.out.println(yy);

	
	}

}
