package in.connect2tech.java.selenium.module2.arrays;

public class ArraysExample {

	public static void main(String[] args) {
		
		int a1;
		int a2;
		int a3;
		
		int arr [] = new int[100];

		// int a [] = new int [5];
		int[] a = new int[5];//Default values in Java
		
		int b [] = {100,200,300,400,500};
		
		int c [] = new int [] {10,20};

		// a[0] = 10;
		// a[1] = 20;

		for (int i = 0; i < 5; i++) {
			a[i] = i + 10;
			System.out.println(a[i]);
		}

		//System.out.println(b.length);
		
		System.out.println("--------------------------------------");
		
		for(int j=0;j<b.length;j++) {
			System.out.println(b[j]);
		}

	}

}
