package in.connect2tech.java.selenium.module2.functions;

public class Calculator {
	
	static void add(int a, int b) {
		System.out.println(a+b);
	}
	
	static void add(String a, String b) {
		System.out.println(a+b);
	}
	
	static void add(int a, int b, int c) {
		System.out.println(a+b+c);
	}

	public static void main(String[] args) {

		add(10,20);
		add(10,20,30);
		
		add("Hello"," Java");
		
		main(10);
		
	}

	
	public static void main(int a) {
		
	}
}
