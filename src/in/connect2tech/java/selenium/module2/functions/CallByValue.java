package in.connect2tech.java.selenium.module2.functions;

public class CallByValue {

	public static void main(String[] args) {
		
		int a = 10;
		int b = 20;
		
		System.out.println(a);
		display(a,b);
		
		
		System.out.println(a);

	}
	
	static void display(int a, int b) {
		
		a = a + 10;
		
		System.out.println("---inside display----");
		System.out.println(a);
	}

}
