package in.connect2tech.java.selenium.module2.functions;

public class Functions {
	
	static void turnOnCoffeeMachine() {
		System.out.println("turnOnCoffeeMachine");
	}
	
	static String makeCoffee() {
		
		System.out.println("Add water");
		System.out.println("Add Sugar");
		System.out.println("Add Milk");
		System.out.println("Heat / Warm");
		
		return "Coffee";
	}
	
	public static void main(String str[]) {
		turnOnCoffeeMachine();
		System.out.println("-----------------------------");
		String mug1 = makeCoffee();
		String mug2 = makeCoffee();
		String mug3 = makeCoffee();
		
		System.out.println(mug1);
		System.out.println(mug2);
		System.out.println(mug3);
	}

}
